package ua.cn.audioremote;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

import javax.net.ssl.SSLSocket;

import org.apache.http.conn.ssl.SSLSocketFactory;

import ua.cn.audioremote.data.ExchangeData;
import ua.cn.audioremote.data.ExchangeData.Command;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.perm.kate.api.Audio;

public class SocketClient extends Thread {

	// options
	private InputStream audioremouteclient;
	private final static String TS_PASS = "q1w2e3";
	private String serverName = "";
	private int port = 5434;
	private Handler handler;
	private Context context;
	// comand
	private Command command = Command.START;
	private String url = "";
	private double moveTo = 0.0;
	private double volume = 0.0;

	private SSLSocket client = null;
	private ArrayList<Audio> listAudioInfoA;

	public SocketClient(Handler h,Context context, InputStream audioremouteclient,
			String serverName, int port) {
		super();
		this.audioremouteclient = audioremouteclient;
		this.serverName = serverName;
		this.port = port;
		this.handler = h;
		this.context=context;
	}

	@Override
	public void interrupt() {
		// TODO Auto-generated method stub
		super.interrupt();
		if (client != null) {
			try {
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		try {			
			KeyStore localKeyStore = KeyStore.getInstance("BKS");
			localKeyStore.load(audioremouteclient, TS_PASS.toCharArray());
			
			SSLSocketFactory sslsocketfactory = new SSLSocketFactory(
					localKeyStore);
			sslsocketfactory
					.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			client = (SSLSocket) sslsocketfactory.createSocket(new Socket(
					serverName, port), serverName, port, false);
			
			ObjectOutputStream oos = new ObjectOutputStream(
					client.getOutputStream());
			ExchangeData ed = new ExchangeData();
			if (command == Command.GETPLAYLIST) {
				System.out.println("---");
				ed.setCom(command);
				oos.writeObject(ed);
				oos.close();
				ObjectInputStream ois = new ObjectInputStream(
						client.getInputStream());
				Object rsO = ois.readObject();
				listAudioInfoA = (ArrayList<Audio>) rsO;
				ois.close();
				client.close();
			} else {
				switch (command) {
				case START:
					ed.setUrl(url);// http://cs1782.vk.me/u22040572/audios/d3d6a746412b.mp3?extra=Z6gT5dxxHpa36bdqm71dU0l3zoIh_KHQM9Huxh3SREwIjyeeuw1X4H8HHCzJajzp9R7ZVPm_XUa6Gt7PtSrR95lJUVleCHgywg
					break;
				case REPEAT:
					ed.setUrl(url);
					break;
				case MOVETO:
					ed.setMoveTo(moveTo);
					break;
				case SETVOLUME:
					ed.setVolume(volume);
					break;
				}
				ed.setCom(command);
				oos.writeObject(ed);
				oos.close();
				client.close();
			}
			
		} catch (UnknownHostException e) {
			System.out.println("Unknown host");
			e.printStackTrace();
		} catch (ConnectException e) {
			System.out.println("Failed to connect to" + serverName + " on  "
					+ port);
			handler.post(new Runnable() {
				@Override
				public void run() {					
					Toast.makeText(context,
							R.string.not_connection_to_ip_server, Toast.LENGTH_SHORT)
							.show();
					
				}
			});
			// e.printStackTrace();
		} catch (IOException e) {
			System.out.println("No I/O");
			e.printStackTrace();
		} catch (KeyStoreException e) {
			System.out.println("Keystore ks error");
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			System.out.println("No such algorithm for ks.load");
			e.printStackTrace();
		} catch (CertificateException e) {
			System.out.println("certificate missing");
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			System.out.println("unrecoverableKeyException");
			e.printStackTrace();
		} catch (KeyManagementException e) {
			System.out.println("key management exception");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	public ArrayList<Audio> getListAudioInfoA() {
		return listAudioInfoA;
	}

	public void SetParam(Command command, String url) {
		this.command = command;
		this.url = url;
	}

	public void SetParam(Command command, double val) {
		this.command = command;
		this.volume = val;
		this.moveTo = val;
	}

}
