package ua.cn.audioremote;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;

import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {
	public static boolean isWifiConnected(Object o) {
		ConnectivityManager cm = (ConnectivityManager) o;
	    NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	    if (wifiInfo != null && wifiInfo.isConnected())
	    {
	        return true;
	    }
	    return false;	
	}
	public static boolean isNetworkConnected(Object o) {
		ConnectivityManager cm = (ConnectivityManager) o;
	    NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	    if (wifiInfo != null && wifiInfo.isConnected())
	    {
	        return true;
	    }
	    wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	    if (wifiInfo != null && wifiInfo.isConnected())
	    {
	        return true;
	    }
	    wifiInfo = cm.getActiveNetworkInfo();
	    if (wifiInfo != null && wifiInfo.isConnected())
	    {
	        return true;
	    }
	    return false;		
	}
	public static boolean  isSocketReachable(String address,int port) {
		try {
			Socket socket =new Socket(address,port);
			socket.close();
			return true;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}		
	
	}
	public static boolean  isHostReachable(String address) {
	    try {
	
	        URL url = new URL(address);
	 
	        HttpURLConnection urlc = 
	            (HttpURLConnection) url.openConnection();
	        urlc.setRequestProperty("User-Agent", "userAgent");
	        urlc.setRequestProperty("Connection", "close");
	        urlc.setConnectTimeout(500);
	        urlc.connect();
	        if (urlc.getResponseCode() == 200) {
	            urlc.disconnect();
	            return true;
	        }
	    } catch (MalformedURLException e) {	    
	        e.printStackTrace();
	        return false;
	    } catch (IOException e) {	    	
	        e.printStackTrace();
	        return false;
	    }
	    return false;
	}
	public static void saveAcount(SharedPreferences settings,String access_token, Long user_id) {
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("access_token", access_token);
		editor.putLong("user_id", user_id);
		editor.commit();
	}
}
