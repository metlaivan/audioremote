package ua.cn.audioremote;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.R.bool;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.perm.kate.api.Api;
import com.perm.kate.api.Auth;

public class StartActivity extends Activity {
	private final String TAG = "StartActivity";
	private final int REQUEST_LOGIN = 1;
	private static final int DIALOG_LIST_SERVER = 1;
	private static final int DIALOG_ADD_SERVER = 2;
	private static final int DIALOG_ABOUT = 3;
	private SharedPreferences settings = null;
	public String access_token;
	public long user_id;
	private Intent logInIntent;
	private Intent vkA ;
	private ArrayList<String> listServers = new ArrayList<String>();
	private static SocketClient socketClient;
	private String serverName = "";
	private int port = 5432;
	final Handler handler= new Handler() ;
	private int idserver;
	Api api;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		getOverflowMenu();

		// Restore preferences
		loadSettings();
		logInIntent = new Intent(this, LogInActivity.class);
		vkA = new Intent(this, VkActivity.class);
		// Если сессия есть создаём API для обращения к серверу
		// if (access_token != null)
		// api = new Api(access_token, Constants.API_ID);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start, menu);
		return true;
	}

	/* onClick methods */

	/** Called when the user touches the Vk button */
	public void clickVKbutton(View view) {
	new isResorseActive().execute();
		
	}

	/** Called when the user touches the Playlist button */
	public void clickPlayListButton(View view) {
new isServerActive().execute();

	}

	/* Override methods */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_LOGIN) {
			if (resultCode == RESULT_OK) {
				// авторизовались успешно
				access_token = data.getStringExtra("token");
				user_id = data.getLongExtra("user_id", 0);
				ua.cn.audioremote.Utils.saveAcount(settings, access_token,
						user_id);
				api = new Api(access_token, Constants.API_ID);
				if (api != null) {
					Intent vkA = new Intent(this, VkActivity.class);
					startActivity(vkA);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_add_new_server:
			Log.i(TAG, "click options");
			// startActivity(optionsIntent);
			showDialog(DIALOG_ADD_SERVER);
			return true;
		case R.id.action_list_servers:
			Log.i(TAG, "click action_list_servers");
			// Toast.makeText(getApplicationContext(), "Ok is pressed",
			// Toast.LENGTH_LONG).show();
			showDialog(DIALOG_LIST_SERVER);
			return true;
		case R.id.action_about:
			Log.i(TAG, "click action_about");
			showDialog(DIALOG_ABOUT);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public Dialog onCreateDialog(int id, Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		switch (id) {
		case DIALOG_LIST_SERVER:
			builder.setTitle(R.string.action_list_server);
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
					android.R.layout.select_dialog_singlechoice, listServers);

			builder.setSingleChoiceItems(arrayAdapter, idserver,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							idserver = which;
							System.out.println(idserver);
						}
					});
			builder.setPositiveButton(R.string.choose,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							SharedPreferences.Editor editor = settings.edit();
							editor.putInt("idserver", idserver);
							StringTokenizer st = new StringTokenizer(
									listServers.get(idserver), ":");
							if (st.countTokens() == 2) {
								serverName=st.nextToken();
								editor.putString("ipserver", serverName);
								port=Integer.valueOf(st.nextToken());
								editor.putInt("portserver",
										port);
							}
							editor.commit();
						}
					});
			builder.setNeutralButton(R.string.delete,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							listServers.remove(idserver);
							int sizeOfListServer = settings.getInt(
									"sizeOfListServer", 0);
							SharedPreferences.Editor editor = settings.edit();
							editor.putString(
									"server" + idserver,
									settings.getString("server"
											+ (sizeOfListServer - 1), ""));
							editor.remove("server" + (sizeOfListServer - 1));
							sizeOfListServer--;
							editor.putInt("sizeOfListServer", sizeOfListServer);
							editor.commit();
						}
					});

			builder.setNegativeButton(R.string.cancel,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});

			break;
		case DIALOG_ADD_SERVER:
			
			builder.setTitle(R.string.action_settings_addServer);
			builder.setView(inflater.inflate(R.layout.dialog_add_server, null))
					// Add action buttons
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int id) {
									EditText etip = (EditText) ((AlertDialog) dialog)
											.findViewById(R.id.ipserver);
									EditText etport = (EditText) ((AlertDialog) dialog)
											.findViewById(R.id.portserver);
									int sizeOfListServer = settings.getInt(
											"sizeOfListServer", 0);
									SharedPreferences.Editor editor = settings
											.edit();
									editor.putString(
											"server"
													+ String.valueOf(sizeOfListServer),
											etip.getText() + ":"
													+ etport.getText());
									listServers.add(etip.getText() + ":"
											+ etport.getText());
									sizeOfListServer++;
									idserver=sizeOfListServer-1;
									editor.putInt("idserver", idserver);
									editor.putString("ipserver", etip.getText().toString());
									editor.putInt("portserver",
											Integer.valueOf(etport.getText().toString()));
									editor.putInt("sizeOfListServer",
											sizeOfListServer);
									port=Integer.valueOf(etport.getText().toString());
									serverName=etip.getText().toString();
									// Commit the edits!
									editor.commit();
									etip.setText("");
									etport.setText("");
								}
							})

					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.dismiss();
								}
							});
			break;
			
			case DIALOG_ABOUT:				
				builder.setTitle(R.string.action_about);
				builder.setView(inflater.inflate(R.layout.dialog_about, null))
				// Add action buttons
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int id) {
								dialog.dismiss();
							}
						});

				break;
		default:
			break;
		}

		return builder.create();

	}

	/* private methods */

	private void logOut() {
		api = null;
		access_token = null;
		user_id = 0;
		ua.cn.audioremote.Utils.saveAcount(settings, access_token, user_id);

	}

	/**
	 * hack for how option menu in Actionbar
	 */
	private void getOverflowMenu() {

		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadSettings() {
		settings = getSharedPreferences(Constants.PREFS_NAME, 0);
		int sizeOfListServer = settings.getInt("sizeOfListServer", 0);
		for (int i = 0; i < sizeOfListServer; i++) {
			listServers.add(settings.getString("server" + i, ""));
		}
		idserver = settings.getInt("idserver", 0);
		access_token = settings.getString("access_token", null);
		user_id = settings.getLong("user_id", 0);
		serverName = settings.getString("ipserver", "");
		port = settings.getInt("portserver", 0);

	}
	public class isResorseActive extends AsyncTask<Void, Void, Void> {
		private boolean vkserver=false;
		private boolean serverIp=false;
	

		@Override
		protected void onPostExecute(Void result) {		
			if (ua.cn.audioremote.Utils
					.isWifiConnected(getSystemService(Context.CONNECTIVITY_SERVICE))) {		
				if (ua.cn.audioremote.Utils
						.isNetworkConnected(getSystemService(Context.CONNECTIVITY_SERVICE))) {		
					if(vkserver){
						if(serverIp){						
								startActivityForResult(logInIntent, REQUEST_LOGIN);					
						}else{
							Toast.makeText(getApplicationContext(), R.string.not_connection_to_ip_server,
									Toast.LENGTH_SHORT).show();
						}
					}else{
						Toast.makeText(getApplicationContext(), R.string.not_connection_to_VK_server,
								Toast.LENGTH_SHORT).show();
					}
				}else{
					Toast.makeText(getApplicationContext(), R.string.not_connection,
							Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(getApplicationContext(), R.string.enable_wifi,
						Toast.LENGTH_SHORT).show();
			}	
		
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... params) {
			vkserver=ua.cn.audioremote.Utils.isHostReachable("http://www.vk.com");
			serverIp=ua.cn.audioremote.Utils.isSocketReachable(serverName,port);			
			return null;
		}
		
	}
	public class isServerActive extends AsyncTask<Void, Void, Void> {
		private boolean serverIp=false;
	

		@Override
		protected void onPostExecute(Void result) {			
			if (ua.cn.audioremote.Utils
					.isWifiConnected(getSystemService(Context.CONNECTIVITY_SERVICE))) {		
					if(serverIp){							
						vkA.putExtra("flPlaylist", true);
						startActivity(vkA);										
						}else{
							Toast.makeText(getApplicationContext(), R.string.not_connection_to_ip_server,
									Toast.LENGTH_SHORT).show();
						}				
			} else {
				Toast.makeText(getApplicationContext(), R.string.enable_wifi,
						Toast.LENGTH_SHORT).show();
			}	
		
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... params) {
				serverIp=ua.cn.audioremote.Utils.isSocketReachable(serverName,port);			
			return null;
		}
		
	}
}
