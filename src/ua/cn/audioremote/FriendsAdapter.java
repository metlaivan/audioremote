package ua.cn.audioremote;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.perm.kate.api.User;

public class FriendsAdapter extends BaseAdapter {
	private static final String TAG = "FriendsAdapter";
	private Activity activity;
	private ArrayList<User> listUser;
	private static LayoutInflater inflater = null;
	private User user;
	
	public FriendsAdapter(Activity activity, ArrayList<User> listUser) {
		super();
		this.activity = activity;		
		this.listUser = listUser;		
	}
	
    static class ViewHolder {
        protected TextView name;
        protected ImageView img;
    }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listUser.size();
	}

	@Override
	public Object getItem(int position) {
		// System.out.println("hi from getItem");
		return position;
	}

	@Override
	public long getItemId(int position) {
		// System.out.println("hi from getItemId");
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = null;
		if (convertView == null){
			inflater=activity.getLayoutInflater();
			vi = inflater.inflate(R.layout.list_friend_row, null);
		final ViewHolder holder=new ViewHolder();
		holder.name = (TextView) vi.findViewById(R.id.name); // name
		holder.img=(ImageView)vi.findViewById(R.id.img_avatar);		
		vi.setTag(holder);
		}else{
			vi=convertView;					
		}
		user = listUser.get(position);
		//System.out.println(user.last_name);
		// Setting all values in listview
		final ViewHolder holder=(ViewHolder) vi.getTag();
		if(position==0){
			holder.name.setText(R.string.mymusic);
		}else{
		holder.name.setText(user.first_name +" "+ user.last_name);
		}
		if (holder.img != null) {		
            new ImageDownloaderTask(holder.img).execute(user.photo);        
		}
		
		return vi;
	}
	
	

}
