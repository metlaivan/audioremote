package ua.cn.audioremote;

import com.perm.kate.api.Auth;

import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressLint("SetJavaScriptEnabled")
public class LogInActivity extends Activity {
	private final String TAG = "LogInActivity";
	private WebView logInWebView;
	private String access_token;
	public static final String PREFS_NAME = "AudioRemouteSettings";

	private SharedPreferences settings = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_in);
		Log.i(TAG, "Start LogInActivity");
		logInWebView = (WebView) findViewById(R.id.logInWebView);
		logInWebView.getSettings().setJavaScriptEnabled(true);
		logInWebView.clearCache(true);
		logInWebView.setWebViewClient(new VkontakteWebViewClient());
		settings = getSharedPreferences(PREFS_NAME, 0);
		access_token = settings.getString("access_token", null);
		if (access_token == null) {
			// otherwise CookieManager will fall with
			// java.lang.IllegalStateException:
			// CookieSyncManager::createInstance() needs to be called before
			// CookieSyncManager::getInstance()
			CookieSyncManager.createInstance(this);
			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.removeAllCookie();
		}
		String url = Auth.getUrl(Constants.API_ID, "friends,audio");
		logInWebView.loadUrl(url);
	}

	@Override
	protected void onStart() {
		if (access_token == null) {
			this.setVisible(true);
		} else {
			this.setVisible(false);
		}
		super.onStart();
	}

	private class VkontakteWebViewClient extends WebViewClient {

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			parseUrl(url);
		}
	}

	private void parseUrl(String url) {
		try {
			if (url == null)
				return;
			if (url.startsWith(Auth.redirect_url)) {
				if (!url.contains("error=")) {
					String[] auth = Auth.parseRedirectUrl(url);
					Intent intent = new Intent();
					intent.putExtra("token", auth[0]);
					intent.putExtra("user_id", Long.parseLong(auth[1]));
					setResult(Activity.RESULT_OK, intent);
				}
				finish();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
