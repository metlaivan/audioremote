package ua.cn.audioremote;

import java.util.ArrayList;

import com.perm.kate.api.Audio;

import android.R.bool;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AudioAdapter extends BaseAdapter {
	private Activity activity;
	private ArrayList<Audio> listAudio;
	private static LayoutInflater inflater = null;
	private Audio audio;
	public int myClickedPosition=-1;
	public int countclick;
	public AudioAdapter(Activity activity, ArrayList<Audio> listAudio) {
		super();
		this.activity = activity;
		
		this.listAudio = listAudio;
		//inflater = (LayoutInflater) activity
		//		.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
    static class ViewHolder {
        protected TextView title;
        protected TextView artist;
        protected TextView duration;
        protected ImageView img;
    }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listAudio.size();
	}

	@Override
	public Object getItem(int position) {
		// System.out.println("hi from getItem");
		return position;
	}

	@Override
	public long getItemId(int position) {
		// System.out.println("hi from getItemId");

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = null;
	if (convertView == null){
			inflater=activity.getLayoutInflater();
			vi = inflater.inflate(R.layout.list_audio_row, null);
		final ViewHolder holder=new ViewHolder();
		holder.title = (TextView) vi.findViewById(R.id.title); // title
		holder.artist = (TextView) vi.findViewById(R.id.artist); // artist		
		holder.duration = (TextView) vi.findViewById(R.id.duration); // duration
		holder.img=(ImageView)vi.findViewById(R.id.img_play_pause);
		
		vi.setTag(holder);
		}else{
			vi=convertView;					
		}

		audio = listAudio.get(position);
		// Setting all values in listview
		ViewHolder holder=(ViewHolder) vi.getTag();
		holder.title.setText(audio.title);
		holder.artist.setText(audio.artist);
		holder.duration.setText(audio.durationInS);
		if(myClickedPosition == position){
			if(countclick==1){
			holder.img.setImageResource(R.drawable.pause);
			}
			else {
				holder.img.setImageResource(R.drawable.play);
			}
		} else  {
			holder.img.setImageResource(R.drawable.play);		
		} 
		
		return vi;
	}



}
