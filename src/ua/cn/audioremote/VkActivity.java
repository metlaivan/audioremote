package ua.cn.audioremote;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import ua.cn.audioremote.R.id;
import ua.cn.audioremote.data.ExchangeData.Command;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.perm.kate.api.Api;
import com.perm.kate.api.Audio;
import com.perm.kate.api.KException;
import com.perm.kate.api.User;

@SuppressLint("ValidFragment")
public class VkActivity extends FragmentActivity implements
		ActionBar.TabListener {
	private static final String TAG = "VkActivity";
	private static final int REQUEST_LOGIN = 0;
	private static final int REQUEST_ReLOGIN = 1;
	private String access_token;
	private Api api;
	public long user_id;
	private static ArrayList<Audio> listAudio;
	private static ArrayList<User> listFriends;
	private SharedPreferences settings = null;
	private Intent logInIntent;
	private static FriendsAdapter friendsAdapter = null;
	private AudioAdapter audioadapter = null;
	private String serverName = "";
	private int port = 5432;
	private static SocketClient socketClient;
	private int currPlayPosition = 0;
	private int currTabPosition = -1;
	private int error_code = -1;
	private boolean flPlay = false;
	private boolean start = false;
	private boolean flMoveTab = false;
	private boolean flRepeat = false;
	private boolean flShuffle = false;
	private boolean flShowPlayer = false;
	private boolean flPlaylist = false;
	private Timer t;
	public int time = 0;
	private TextView curTime;
	private TextView allTime;
	private SeekBar currentDuration;
	private TextView title;
	private TextView artist;
	private ImageButton bPlay;
	final Handler handler= new Handler() ;
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	static ViewPager mViewPager;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vk);
	
		logInIntent = new Intent(this, LogInActivity.class);
		flPlaylist = getIntent().getBooleanExtra("flPlaylist", false);
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		loadSettings();
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
						currTabPosition = position;
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));

		}

		// current Audio
		mViewPager.setCurrentItem(1);
		GetAudioList gal = new GetAudioList(this, user_id);
		gal.execute();
		if (!flPlaylist) {
			GetFriendsList gfl = new GetFriendsList(this);
			gfl.execute();
		}
		// socketClient = new SocketClient(getResources().openRawResource(
		// R.raw.audioremouteclient), serverName, port);
	}

	@Override
	public void finishActivity(int requestCode) {
		// TODO Auto-generated method stub
		super.finishActivity(requestCode);
		Log.i(TAG, "finsish a");
	}

	@Override
	protected void onDestroy() {
		sendComand(Command.STOP, null);
		Log.i(TAG, "onDestroy");
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		start = true;
		super.onStart();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vk, menu);
		return true;
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
		if (flShowPlayer)
			if (tab.getPosition() == 1 && start) {
				flMoveTab = true;
				setCurentSelectedAudio();
			} else if (tab.getPosition() == 0 && start) {
				bPlay = (ImageButton) findViewById(id.imageButtonPlay);
				setIconBplay();
				setTextVeiw();
				setTitleOfAudio();
			}
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {

	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {

	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {
		public static final String ARG_SECTION_NUMBER = "section_number";

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.

			Fragment fragment = new Fragment();
			switch (position) {
			case 1:
				return fragment = new ListAudioFragment();
			case 2:
				return fragment = new ListFriendsFragment();
			case 0:
				return fragment = new PlayerFragment();
			}

			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, position);

			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			if (flPlaylist)
				return 2;
			else
				return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_player).toUpperCase(l);
			case 1:
				return getString(R.string.title_audio).toUpperCase(l);
			case 2:
				return getString(R.string.title_friends).toUpperCase(l);
			}
			return null;
		}
	}

	public static class ListAudioFragment extends Fragment {

		public ListAudioFragment() {

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			View rootView = inflater.inflate(R.layout.fragment_vk_listaudio,
					null);
			Log.i(TAG, "listaudio");
			return rootView;
		}

	}

	@SuppressLint("NewApi")
	public class ListFriendsFragment extends Fragment {

		public ListFriendsFragment() {

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			View rootView = inflater.inflate(R.layout.fragment_vk_listfriends,
					null);
			Log.i(TAG, "ListFriendsFragment");
			ListView listViewFriends = (ListView) rootView
					.findViewById(R.id.listfriends);
			if (friendsAdapter != null) {
				ProgressBar pb = (ProgressBar) rootView
						.findViewById(R.id.progressBarListFriends);
				pb.setVisibility(View.GONE);
				listViewFriends.setAdapter(friendsAdapter);
			}
			listViewFriends.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapterView, View view,
						int pos, long arg3) {
					Log.i(TAG, "click friend " + pos);
					long userid = listFriends.get(pos).uid;
					GetAudioList gal = new GetAudioList(getActivity(), userid);
					gal.execute();
					mViewPager.setCurrentItem(1);
				}
			});

			return rootView;
		}

	}

	public class PlayerFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */

		public PlayerFragment() {

		}

		@Override
		public void onConfigurationChanged(Configuration newConfig) {
			super.onConfigurationChanged(newConfig);
			LayoutInflater inflater = LayoutInflater.from(getActivity());
			populateViewForOrientation(inflater, (ViewGroup) getView());
		}

		private void populateViewForOrientation(LayoutInflater inflater,
				ViewGroup viewGroup) {
			viewGroup.removeAllViewsInLayout();
			View subview = inflater.inflate(R.layout.fragment_vk_player,
					viewGroup);
			initLayout(subview);
			bPlay = (ImageButton) findViewById(id.imageButtonPlay);
			setIconBplay();
			setTitleOfAudio();
			scheduleQue(time);
		}

		private void initLayout(View view) {
			View rootView = view;
			setTextVeiw();
			curTime = (TextView) rootView.findViewById(id.textViewCurrentTime);
			SeekBar volume = (SeekBar) rootView.findViewById(id.seekBarVolum);
			currentDuration = (SeekBar) rootView
					.findViewById(id.seekBarCurrentDuration);
			// play button
			bPlay = (ImageButton) rootView.findViewById(id.imageButtonPlay);
			final ImageButton bNext = (ImageButton) rootView
					.findViewById(id.imageButtonNext);
			final ImageButton bPrev = (ImageButton) rootView
					.findViewById(id.imageButtonPrevious);
			final ImageButton bRepeat = (ImageButton) rootView
					.findViewById(id.imageButtonRepeat);
			final ImageButton bShuffle = (ImageButton) rootView
					.findViewById(id.imageButtonShuffle);
			final ImageView iVolum = (ImageView) rootView
					.findViewById(id.imageViewVolum);
			setIconBplay();

			bPlay.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (flPlay) {
						bPlay.setImageResource(R.drawable.media_play);
						sendComand(Command.PAUSE, null);
						flPlay = false;
						if (t != null) {
							t.cancel();
							t = null;
						}
					} else {
						bPlay.setImageResource(R.drawable.media_pause);
						sendComand(Command.START,
								listAudio.get(currPlayPosition).url);
						flPlay = true;
						scheduleQue(time);

					}
					setTitleOfAudio();
				}
			});
			bNext.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					currPlayPosition++;
					sendComand(Command.START,
							listAudio.get(currPlayPosition).url);
					flPlay = true;
					setIconBplay();
					scheduleQue(0);
					setTitleOfAudio();
				}

			});
			bPrev.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					currPlayPosition--;
					if (currPlayPosition < 0)
						currPlayPosition = 0;
					sendComand(Command.START,
							listAudio.get(currPlayPosition).url);
					flPlay = true;
					setIconBplay();
					scheduleQue(0);
					setTitleOfAudio();
				}
			});

			volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					Double cur = (double) seekBar.getProgress() / 100;
					sendComand(Command.SETVOLUME, cur);
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					if (progress < 30) {
						iVolum.setImageResource(R.drawable.media_volume_1);
					} else if (progress < 60) {
						iVolum.setImageResource(R.drawable.media_volume_2);
					} else {
						iVolum.setImageResource(R.drawable.media_volume_3);
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub

				}

			});
			currentDuration
					.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
						@Override
						public void onStopTrackingTouch(SeekBar seekBar) {
							// TODO Auto-generated method stub
							time = seekBar.getProgress();
							sendComand(Command.MOVETO, Double.valueOf(time));
						}

						@Override
						public void onStartTrackingTouch(SeekBar seekBar) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onProgressChanged(SeekBar seekBar,
								int progress, boolean fromUser) {

						}
					});

			bRepeat.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					flRepeat = !flRepeat;
					if (flRepeat) {
						bRepeat.setImageResource(R.drawable.media_repeat_old_b);
						bShuffle.setImageResource(R.drawable.media_shuffle);
					} else {
						bRepeat.setImageResource(R.drawable.media_repeat_old);
					}
				}
			});
			bShuffle.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					flShuffle = !flShuffle;
					if (flShuffle && !flRepeat) {
						bShuffle.setImageResource(R.drawable.media_shuffle_b);
					} else {
						bShuffle.setImageResource(R.drawable.media_shuffle);
					}
				}
			});
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			Log.i(TAG, "ListPlayerFragment");
			View rootView = inflater.inflate(R.layout.fragment_vk_player,
					container, false);
			initLayout(rootView);
			return rootView;
		}

	}

	public class GetAudioList extends AsyncTask<Void, Void, Void> {

		private static final String TAG = "GetAudioList";
		private Activity activity;
		private int Countclick = 0;
		private int posclick = -1;
		private long userid;
		private boolean flShowToast = false;

		public GetAudioList(Activity activity, long userid) {
			super();
			this.activity = activity;
			this.userid = userid;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ProgressBar pb = (ProgressBar) findViewById(R.id.progressBarInListAudio);
			ListView listViewAudio = (ListView) findViewById(R.id.listAudio);
			if (pb != null) {
				pb.setVisibility(View.VISIBLE);
				listAudio = new ArrayList<Audio>();
				audioadapter = new AudioAdapter(activity, listAudio);
				listViewAudio.setAdapter(audioadapter);

			}

		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			ProgressBar pb = (ProgressBar) findViewById(R.id.progressBarInListAudio);
			if (pb != null)
				pb.setVisibility(View.GONE);
			ListView listViewAudio = (ListView) findViewById(R.id.listAudio);

			if (listAudio == null) {
				listAudio = new ArrayList<Audio>();
			}
			audioadapter = new AudioAdapter(activity, listAudio);
			if (listViewAudio != null) {
				listViewAudio.setAdapter(audioadapter);
				listViewAudio.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> adapterView,
							View view, int position, long arg3) {
						// TODO Auto-generated method stub

						currPlayPosition = position;
						AudioAdapter madapter = (AudioAdapter) adapterView
								.getAdapter();
						if (!flMoveTab) {
							if (posclick == position || posclick == -1) {
								Countclick++;
							} else {
								Countclick = 1;
								time = 0;
							}
							if (Countclick == 2) {
								Countclick = 0;
							}

							if (Countclick == 1) {
								sendComand(Command.START,
										listAudio.get(currPlayPosition).url);
								flPlay = true;
								scheduleQue(time);
							}
							if (Countclick == 0) {
								sendComand(Command.PAUSE, null);
								if (t != null) {
									t.cancel();
									t = null;
								}
								flPlay = false;
							}
						} else {
							flMoveTab = false;
							if (flPlay) {
								Countclick = 1;
							} else {
								Countclick = 0;
							}
						}
						// countclick=1 - play
						// countclick=0 pause

						madapter.myClickedPosition = position;
						madapter.countclick = Countclick;
						posclick = position;
						madapter.notifyDataSetInvalidated();
					}
				});
			}
			if (flShowToast) {
				if (error_code == 201) {
					Toast.makeText(getApplicationContext(),
							R.string.access_denie_to_audio, Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(getApplicationContext(),
							R.string.not_connection, Toast.LENGTH_SHORT).show();
				}
				flShowToast = false;
			}
			flShowPlayer = true;
			System.out.println(listAudio);
			if(flPlaylist && listAudio.size()==0)				
					Toast.makeText(getApplicationContext(), R.string.playlist_not_load_to_server,
							Toast.LENGTH_SHORT).show();
				
		}

		@Override
		protected Void doInBackground(Void... params) {
			if (flPlaylist) {
				getListAudio();
			} else {
				getVklistAudio();
			}
			return null;

		}

		private void getListAudio() {
//			 socketClient = new SocketClient(handler,getApplicationContext(),getResources().openRawResource(
//						R.raw.audioremouteclient), serverName, port);
//			 socketClient.SetParam(Command.GETPLAYLIST,
//			 null);
//			 socketClient.start();
	
				
			//System.out.println("start");
			sendComand(Command.GETPLAYLIST, null);

	
		while(true){
				if (!socketClient.isAlive()) {
					listAudio = socketClient.getListAudioInfoA();
				break;
				}
		}
		
			//System.out.println("finish");

		}

		private void getVklistAudio() {
			if (access_token != null)
				api = new Api(access_token, Constants.API_ID);

			if (api != null) {
				try {
					ArrayList<Audio> list = api.getAudio(this.userid, null,
							null, null, null, null);

					listAudio = list;

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (KException e) {
					Log.i(TAG, "KException");
					if (e.error_code == 5) {
						clickLogoutButton();
					}
					if (e.error_code == 201) {
						flShowToast = true;
						error_code = e.error_code;

					} else {
						startActivityForResult(logInIntent, REQUEST_LOGIN);
						getVklistAudio();
					}

				} catch (IOException e) {
					flShowToast = true;
					// TODO Auto-generated catch block
					// Toast.makeText(getApplicationContext(),
					// R.string.not_connection, Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}
			}
		}

	}

	public class GetFriendsList extends AsyncTask<Void, Void, Void> {

		private static final String TAG = "GetFrinedsList";
		private Activity activity;
		private boolean flShowToast = false;

		public GetFriendsList(Activity activity) {
			super();
			this.activity = activity;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ProgressBar pb = (ProgressBar) findViewById(R.id.progressBarListFriends);
			if (pb != null)
				pb.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			ListView listViewFriends = (ListView) findViewById(R.id.listfriends);

			if (listFriends == null) {
				listFriends = new ArrayList<User>();
			}
			friendsAdapter = new FriendsAdapter(activity, listFriends);
			if (listViewFriends != null)
				listViewFriends.setAdapter(friendsAdapter);
			ProgressBar pb = (ProgressBar) findViewById(R.id.progressBarListFriends);
			if (pb != null)
				pb.setVisibility(View.GONE);
			if (flShowToast) {
				Toast.makeText(getApplicationContext(),
						R.string.not_connection, Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			getlistFriends();
			return null;

		}

		private void getlistFriends() {
			if (access_token != null)
				api = new Api(access_token, Constants.API_ID);
			if (api != null) {
				try {
					listFriends = api.getFriends(user_id, "photo_50", null,
							null, null);
					ArrayList<Long> ulist = new ArrayList<Long>();
					ulist.add(user_id);
					ArrayList<User> userI = api.getProfiles(ulist, null,
							"photo_50", null, null, null);
					listFriends.add(0, userI.get(0));
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					flShowToast = true;
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (KException e) {
					Log.i(TAG, "KException");

					startActivityForResult(logInIntent, REQUEST_LOGIN);
					getlistFriends();
				}

			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_LOGIN) {
			if (resultCode == RESULT_OK) {
				// авторизовались успешно
				access_token = data.getStringExtra("token");
				user_id = data.getLongExtra("user_id", 0);
				ua.cn.audioremote.Utils.saveAcount(settings, access_token,
						user_id);
				api = new Api(access_token, Constants.API_ID);
			}
		} else if (requestCode == REQUEST_ReLOGIN) {
			if (resultCode == RESULT_OK) {
				access_token = data.getStringExtra("token");
				user_id = data.getLongExtra("user_id", 0);
				ua.cn.audioremote.Utils.saveAcount(settings, access_token,
						user_id);
				api = new Api(access_token, Constants.API_ID);
				if (api != null) {
					Intent vkA = new Intent(this, VkActivity.class);
					startActivity(vkA);
				}
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_logout:
			clickLogoutButton();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* onClick methods */
	/** Called when the user touches the Logout button */
	public void clickLogoutButton() {
		logOut();
		startActivityForResult(logInIntent, REQUEST_ReLOGIN);
	}

	/* private methods */
	private void loadSettings() {
		settings = getSharedPreferences(Constants.PREFS_NAME, 0);
		access_token = settings.getString("access_token", null);
		user_id = settings.getLong("user_id", 0);
		serverName = settings.getString("ipserver", "");
		port = settings.getInt("portserver", 0);
		// System.out.println(serverName + "  "+port);
	}

	private void logOut() {
		api = null;
		access_token = null;
		user_id = 0;
		ua.cn.audioremote.Utils.saveAcount(settings, access_token, user_id);

	}

	private void sendComand(Command c, Object o) {
		socketClient = new SocketClient(handler,getApplicationContext(),getResources().openRawResource(
				R.raw.audioremouteclient), serverName, port);
		if (o instanceof String)
			socketClient.SetParam(c, (String) o);
		else if (o instanceof Double) {
			socketClient.SetParam(c, (Double) o);
		} else
			socketClient.SetParam(c, null);
		socketClient.start();
	}

	private void setTitleOfAudio() {
		if (listAudio != null) {
			if (listAudio.size() != 0) {
				title.setText(listAudio.get(currPlayPosition).title);
				artist.setText(listAudio.get(currPlayPosition).artist);
				allTime.setText(listAudio.get(currPlayPosition).durationInS);
			}
		}
	}

	private void setIconBplay() {
		if (flPlay) {
			bPlay.setImageResource(R.drawable.media_pause);
		} else {
			bPlay.setImageResource(R.drawable.media_play);
		}
	}

	private void setTextVeiw() {
		title = (TextView) findViewById(id.textViewTitle);
		artist = (TextView) findViewById(id.textViewArtist);
		allTime = (TextView) findViewById(id.textViewAllTime);
	}

	private void setCurentSelectedAudio() {
		ListView listViewAudio = (ListView) findViewById(R.id.listAudio);
		// Log.i(TAG, listViewAudio.toString());
		if (listAudio.size() != 0)
			listViewAudio.performItemClick(
					listViewAudio.getAdapter().getView(currPlayPosition, null,
							null), currPlayPosition, listViewAudio.getAdapter()
							.getItemId(currPlayPosition));
		listViewAudio.setSelection(currPlayPosition);
		// listViewAudio.smoothScrollToPosition(currPlayPosition);
	}

	private void scheduleQue(int curtime) {
		if (t != null) {
			time = curtime;
			t.cancel();
			t = new Timer();
		} else {
			t = new Timer();
		}
		currentDuration.setMax(0);
		currentDuration.setMax((int) listAudio.get(currPlayPosition).duration);
		t.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (listAudio.size() != 0)
							if (time <= listAudio.get(currPlayPosition).duration) {
								curTime.setText(String.valueOf(time / 60)
										+ ":"
										+ (String.valueOf(time % 60).length() == 1 ? ("0" + String
												.valueOf(time % 60)) : String
												.valueOf(time % 60)));
								currentDuration.setProgress(time);
								time += 1;
							} else {
								if (!flRepeat)
									currPlayPosition++;
								if (flShuffle && !flRepeat) {
									currPlayPosition = new Random()
											.nextInt(listAudio.size());
								}
								if (flRepeat) {
									sendComand(Command.REPEAT,
											listAudio.get(currPlayPosition).url);
								} else {
									sendComand(Command.START,
											listAudio.get(currPlayPosition).url);
								}
								setTitleOfAudio();
								setCurentSelectedAudio();
								scheduleQue(0);
							}
					}
				});
			}

		}, 0, 1000);
	}

	
}
